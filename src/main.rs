extern crate anyhow;
extern crate cpal;
extern crate hound;
extern crate midly;

use std::collections::HashMap;
use std::{env, io};

use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    FromSample, SampleFormat, SampleRate, SizedSample,
};
use hound::WavWriter;
use midly::num::{u4, u7};
use midly::{Format, MetaMessage, MidiMessage, Smf, Timing, TrackEvent, TrackEventKind};

fn main() -> anyhow::Result<()> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("Usage: {} <argument>", args[0]);
        std::process::exit(1);
    }

    let raw_midi = std::fs::read(&args[1])?;
    let raw_midi = Box::leak(raw_midi.into_boxed_slice());
    let smf = Smf::parse(raw_midi)?;

    let stream = stream_setup_for(&smf)?;
    stream.play()?;
    std::thread::sleep(std::time::Duration::from_secs(10000));
    Ok(())
}

pub struct ChannelState {
    pub instrument: u8,
}

pub struct TrackState<'a> {
    pub track: Vec<TrackEventWithTrackNumber<'a>>,
    pub current_index: usize,
    pub ticks_until_next_event: Option<u32>,
    pub channels: [ChannelState; 16],
}

impl<'a> TrackState<'a> {
    pub fn find_ticks_until_next_event(
        &mut self,
        tick_length: f32,
        events: &mut Vec<TrackEventWithTrackNumber<'a>>,
    ) -> Option<u32> {
        let event = self.track[self.current_index];
        events.push(event);

        loop {
            self.current_index += 1;
            if self.current_index >= self.track.len() {
                return None;
            }
            let next_event = self.track[self.current_index];
            let next_delta = next_event.delta;
            if next_delta > 0 {
                return Some((tick_length * next_delta as f32).round() as u32);
            }
            events.push(next_event);
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash)]
pub struct TrackEventWithTrackNumber<'a> {
    /// How many MIDI ticks after the previous event should this event fire.
    pub delta: u32,
    /// The type of event along with event-specific data.
    pub kind: TrackEventKind<'a>,
    /// The track number this event belongs to.
    pub track_number: usize,
}

impl<'a> TrackEventWithTrackNumber<'a> {
    pub fn new(track_number: usize, event: &TrackEvent<'a>) -> TrackEventWithTrackNumber<'a> {
        TrackEventWithTrackNumber {
            delta: event.delta.as_int(),
            kind: event.kind.clone(),
            track_number,
        }
    }
}

impl<'a> TrackState<'a> {
    fn new(track: Vec<TrackEventWithTrackNumber<'a>>) -> TrackState {
        let ticks_until_next_event = if track.is_empty() {
            None
        } else {
            Some(track[0].delta)
        };
        Self {
            track,
            current_index: 0,
            ticks_until_next_event,
            channels: [0u8; 16].map(|_i| ChannelState { instrument: 0 }),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct PlayedNoteKey {
    track_number: usize,
    channel: u4,
    note: u7,
}

pub struct PlayedNote {
    start_frame: u32,
    current_vel: f32,
    target_vel: f32,
}

pub struct Oscillator<'a, W>
where
    W: io::Write + io::Seek,
{
    pub tracks: Vec<TrackState<'a>>,
    pub sample_rate: f32,
    pub frame_index: u32,
    pub num_channels: usize,
    pub midi_freq_table: MidiFreqTable,
    pub timing: Timing,
    pub played_notes: HashMap<PlayedNoteKey, PlayedNote>,
    pub tick_length: f32,
    pub max_amp: f32,
    pub wav_writer: WavWriter<W>,
}

const BASE_FREQ: f32 = 440.0;
const BASE_MIDI_NUMBER: usize = 69;
const NUM_MIDI_NOTES: usize = 127;

#[derive(Copy, Clone)]
pub struct MidiFreq {
    pub frequency: f32,
    pub gain_correction: f32,
}

pub type MidiFreqTable = [MidiFreq; NUM_MIDI_NOTES];

fn create_midi_freq_table() -> MidiFreqTable {
    let sqrt_base_freq = f32::sqrt(midi_freq(0));

    let mut result: MidiFreqTable = [MidiFreq {
        frequency: 0.0,
        gain_correction: 0.0,
    }; NUM_MIDI_NOTES];

    for i in 0..NUM_MIDI_NOTES {
        let frequency = midi_freq(i);
        let gain_correction = sqrt_base_freq / f32::sqrt(frequency);
        result[i] = MidiFreq {
            frequency,
            gain_correction,
        };
    }

    result
}

fn midi_freq(i: usize) -> f32 {
    BASE_FREQ * 2f32.powf((i as f32 - BASE_MIDI_NUMBER as f32) / 12f32)
}

impl<W: io::Seek + io::Write> Oscillator<'_, W> {
    fn build_output<SampleType>(&mut self, output: &mut [SampleType])
    where
        SampleType: SizedSample + FromSample<f32>,
    {
        let fade: f32 = 375f32 / self.sample_rate; // 1/128 at 48kHz

        for frame in output.chunks_mut(self.num_channels) {
            self.advance();

            self.played_notes.iter_mut().for_each(|(note_key, note)| {
                let vel_diff = note.target_vel - note.current_vel;
                if vel_diff != 0f32 {
                    if vel_diff.abs() <= fade {
                        note.current_vel = note.target_vel;
                    } else if vel_diff > 0f32 {
                        note.current_vel += fade;
                    } else {
                        note.current_vel -= fade;
                    }
                }
            });

            self.played_notes
                .retain(|_note_key, note| note.current_vel != 0f32);

            let sample = self
                .played_notes
                .iter()
                .map(|(note_key, note)| {
                    // let channel = &self.tracks[note_key.track_number].channels[note_key.channel.as_int() as usize];
                    // let instrument = channel.instrument;
                    let midi = note_key.note.as_int() as usize;
                    let MidiFreq {
                        frequency,
                        gain_correction,
                    } = self.midi_freq_table[midi];
                    let sample = self.sine_wave(frequency);
                    let gain = note.current_vel / 2f32;

                    sample * gain * gain_correction
                })
                .sum();

            for ch in 0..frame.len() {
                frame[ch] = SampleType::from_sample(sample)
            }

            let _ = self
                .wav_writer
                .write_sample((sample * i16::MAX as f32).round() as i16);

            if sample.abs() > self.max_amp {
                self.max_amp = sample.abs();
                // println!("Max amp: {}", self.max_amp);
            }
        }
    }

    fn advance(&mut self) {
        self.frame_index += 1;

        let mut events: Vec<TrackEventWithTrackNumber> = vec![];

        for track_state in self.tracks.iter_mut() {
            track_state.ticks_until_next_event = match track_state.ticks_until_next_event {
                Some(0) => track_state.find_ticks_until_next_event(self.tick_length, &mut events),
                Some(ticks) => Some(ticks - 1),
                None => None,
            };
        }

        for event in events {
            self.evict(event);
        }
    }

    fn evict(&mut self, event: TrackEventWithTrackNumber) {
        // println!("F{} {:?}", self.frame_index, event);
        match event.kind {
            TrackEventKind::Midi {
                channel,
                message: MidiMessage::NoteOn { key, vel },
            } => {
                let key = PlayedNoteKey {
                    track_number: event.track_number,
                    channel,
                    note: key,
                };

                let option = self.played_notes.get_mut(&key);
                match option {
                    Some(existing) => existing.target_vel = vel_to_f32(vel),
                    None => {
                        if vel != 0 {
                            self.played_notes.insert(
                                key,
                                PlayedNote {
                                    start_frame: self.frame_index,
                                    current_vel: 0f32,
                                    target_vel: vel_to_f32(vel),
                                },
                            );
                        }
                    }
                }
            }
            TrackEventKind::Midi {
                channel,
                message: MidiMessage::NoteOff { key, vel: _vel },
            } => {
                self.played_notes
                    .get_mut(&PlayedNoteKey {
                        track_number: event.track_number,
                        channel,
                        note: key,
                    })
                    .map(|note| note.target_vel = 0f32);
            }
            TrackEventKind::Midi {
                channel,
                message: MidiMessage::ChannelAftertouch { vel },
            } => {
                self.played_notes.iter_mut().for_each(|(note_key, note)| {
                    if note_key.track_number == event.track_number && note_key.channel == channel {
                        note.target_vel = vel_to_f32(vel);
                    }
                });
            }
            TrackEventKind::Midi {
                channel,
                message: MidiMessage::Aftertouch { key, vel },
            } => {
                self.played_notes
                    .get_mut(&PlayedNoteKey {
                        track_number: event.track_number,
                        channel,
                        note: key,
                    })
                    .map(|note| note.target_vel = vel_to_f32(vel));
            }
            TrackEventKind::Midi {
                channel,
                message: MidiMessage::ProgramChange { program },
            } => {
                self.tracks[event.track_number].channels[channel.as_int() as usize].instrument =
                    program.as_int()
            }

            TrackEventKind::Midi {
                channel: _channel,
                message: MidiMessage::PitchBend { bend: _bend },
            } => {
                eprintln!(
                    "F{} T{} not supported: {:?}",
                    self.frame_index, event.track_number, event.kind
                )
            }
            TrackEventKind::Midi {
                channel: _channel,
                message:
                    MidiMessage::Controller {
                        controller: _controller,
                        value: _value,
                    },
            } => {
                eprintln!(
                    "F{} T{} not supported: {:?}",
                    self.frame_index, event.track_number, event.kind
                )
            }
            TrackEventKind::Meta(MetaMessage::TrackNumber(_)) => {}
            TrackEventKind::Meta(MetaMessage::Text(text)) => {
                println!(
                    "F{} T{} Text: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(text)
                )
            }
            TrackEventKind::Meta(MetaMessage::Copyright(copyright)) => {
                println!(
                    "F{} T{} Copyright: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(copyright)
                )
            }
            TrackEventKind::Meta(MetaMessage::TrackName(track_name)) => {
                println!(
                    "F{} T{} Track name: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(track_name)
                )
            }
            TrackEventKind::Meta(MetaMessage::InstrumentName(instruments_name)) => {
                println!(
                    "F{} T{} Instrument name: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(instruments_name)
                )
            }
            TrackEventKind::Meta(MetaMessage::Lyric(lyric)) => {
                println!(
                    "F{} T{} Lyric: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(lyric)
                )
            }
            TrackEventKind::Meta(MetaMessage::Marker(marker)) => {
                println!(
                    "F{} T{} Marker: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(marker)
                )
            }
            TrackEventKind::Meta(MetaMessage::CuePoint(cue_point)) => {
                println!(
                    "F{} T{} Cue point: {}",
                    self.frame_index,
                    event.track_number,
                    bytes_to_string(cue_point)
                )
            }
            TrackEventKind::Meta(MetaMessage::ProgramName(program_name)) => {
                println!(
                    "F{} T{} Program name: {:?}",
                    self.frame_index, event.track_number, program_name
                )
            }
            TrackEventKind::Meta(MetaMessage::DeviceName(device_name)) => {
                println!(
                    "F{} T{} Device name: {:?}",
                    self.frame_index, event.track_number, device_name
                )
            }
            TrackEventKind::Meta(MetaMessage::MidiChannel(_)) => {}
            TrackEventKind::Meta(MetaMessage::MidiPort(_)) => {}
            TrackEventKind::Meta(MetaMessage::Tempo(micros_per_beat)) => {
                self.tick_length = self.sample_rate
                    / ticks_per_second(micros_per_beat.as_int() as f32, &self.timing);
            }
            TrackEventKind::Meta(MetaMessage::SmpteOffset(_)) => {}
            TrackEventKind::Meta(MetaMessage::TimeSignature(_, _, _, _)) => {}
            TrackEventKind::Meta(MetaMessage::KeySignature(_, _)) => {}
            TrackEventKind::Meta(MetaMessage::SequencerSpecific(_)) => {}

            TrackEventKind::SysEx(data) => {
                println!(
                    "F{} T{} SysEx event: {:?}",
                    self.frame_index, event.track_number, data
                )
            }

            TrackEventKind::Escape(data) => {
                println!(
                    "F{} T{} Escape event: {:?}",
                    self.frame_index, event.track_number, data
                )
            }
            TrackEventKind::Meta(MetaMessage::EndOfTrack) => {}
            TrackEventKind::Meta(MetaMessage::Unknown(_, _)) => {}
        }
    }

    fn sine_wave(&self, frequency_hz: f32) -> f32 {
        let two_pi = 2.0 * std::f32::consts::PI;
        (self.frame_index as f32 * frequency_hz * two_pi / self.sample_rate).sin()
    }
}

fn vel_to_f32(vel: u7) -> f32 {
    vel.as_int() as f32 / 127f32
}

fn bytes_to_string(bytes: &[u8]) -> &str {
    match std::str::from_utf8(&bytes) {
        Ok(ascii_str) => ascii_str,
        _ => "",
    }
}

pub fn stream_setup_for(smf: &Smf<'static>) -> Result<cpal::Stream, anyhow::Error> {
    let device = cpal::default_host()
        .default_output_device()
        .ok_or_else(|| anyhow::Error::msg("Default output device is not available"))?;
    println!("Output device : {}", device.name()?);

    let config = device.default_output_config()?;
    println!("Default output config : {:?}", config);

    match config.sample_format() {
        SampleFormat::I8 => make_stream::<i8>(&device, &config.into(), smf),
        SampleFormat::I16 => make_stream::<i16>(&device, &config.into(), smf),
        SampleFormat::I32 => make_stream::<i32>(&device, &config.into(), smf),
        SampleFormat::I64 => make_stream::<i64>(&device, &config.into(), smf),
        SampleFormat::U8 => make_stream::<u8>(&device, &config.into(), smf),
        SampleFormat::U16 => make_stream::<u16>(&device, &config.into(), smf),
        SampleFormat::U32 => make_stream::<u32>(&device, &config.into(), smf),
        SampleFormat::U64 => make_stream::<u64>(&device, &config.into(), smf),
        SampleFormat::F32 => make_stream::<f32>(&device, &config.into(), smf),
        SampleFormat::F64 => make_stream::<f64>(&device, &config.into(), smf),
        sample_format => Err(anyhow::Error::msg(format!(
            "Unsupported sample format '{sample_format}'"
        ))),
    }
}

pub fn make_stream<T>(
    device: &cpal::Device,
    config: &cpal::StreamConfig,
    smf: &Smf<'static>,
) -> Result<cpal::Stream, anyhow::Error>
where
    T: SizedSample + FromSample<f32>,
{
    let SampleRate(sample_rate) = config.sample_rate;

    let wav_spec = hound::WavSpec {
        channels: 1,
        sample_rate,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    };

    let sample_rate = sample_rate as f32;
    let timing = smf.header.timing;
    let tick_length = sample_rate / ticks_per_second(500_000f32, &timing);
    let tracks = transform_tracks(smf);

    let wav_writer = WavWriter::create("output.wav", wav_spec)?;

    let mut oscillator = Oscillator {
        tracks,
        num_channels: config.channels as usize,
        sample_rate,
        frame_index: 0,
        midi_freq_table: create_midi_freq_table(),
        timing,
        played_notes: HashMap::new(),
        tick_length,
        max_amp: 0f32,
        wav_writer,
    };

    while oscillator.played_notes.is_empty() {
        oscillator.advance()
    }

    let stream = device.build_output_stream(
        config,
        move |output: &mut [T], _: &cpal::OutputCallbackInfo| oscillator.build_output(output),
        |err| eprintln!("Error building output sound stream: {}", err),
        None,
    )?;

    Ok(stream)
}

fn ticks_per_second(micros_per_beat: f32, timing: &Timing) -> f32 {
    println!("Header timing: {:?}", timing);
    let result = match timing {
        Timing::Metrical(ticks_per_beat) => {
            ticks_per_beat.as_int() as f32 * 1_000_000f32 / micros_per_beat
        }
        Timing::Timecode(fps, sf) => fps.as_f32() * *sf as f32,
    };
    println!("Ticks per second: {}", result);
    result
}

fn transform_tracks<'a>(smf: &Smf<'a>) -> Vec<TrackState<'a>> {
    let tracks: Vec<TrackState> = match smf.header.format {
        Format::SingleTrack => Vec::from([TrackState::new(
            smf.tracks[0]
                .iter()
                .map(|event| TrackEventWithTrackNumber::new(0, event))
                .collect::<Vec<TrackEventWithTrackNumber>>(),
        )]),
        Format::Parallel => smf
            .tracks
            .iter()
            .enumerate()
            .map(|(track_number, track)| {
                TrackState::new(
                    track
                        .iter()
                        .map(|event| TrackEventWithTrackNumber::new(track_number, event))
                        .collect::<Vec<TrackEventWithTrackNumber>>(),
                )
            })
            .collect(),
        Format::Sequential => Vec::from([TrackState::new(
            smf.tracks
                .iter()
                .enumerate()
                .flat_map(|(track_number, track)| {
                    track
                        .iter()
                        .map(move |event| TrackEventWithTrackNumber::new(track_number, event))
                })
                .collect::<Vec<TrackEventWithTrackNumber>>(),
        )]),
    };
    tracks
}
