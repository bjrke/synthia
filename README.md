# synthia

A Software Synthesizer written in Rust.

Work in progress.

## Getting started

install Rust: https://rustup.rs/

```shell
cargo run -- your_midi_file.mid
```

## License
The License is AGPLv3.0. See the LICENSE file for the full license text.

## Where to get some midi from?

https://legendofmi.com/downloads/midis/

## The output.wav is corrupted, how can I repair it?

```shell
ffmpeg -i output.wav repaired.wav
```
